﻿namespace SignalRAppAPI.Models
{
    public class NotificationCountResult
    {
        public int Count { get; set; }
    }
}
