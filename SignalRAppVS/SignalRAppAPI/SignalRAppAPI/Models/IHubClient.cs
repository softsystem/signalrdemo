﻿using System.Threading.Tasks;

namespace SignalRAppAPI.Models
{
    public interface IHubClient
    {
        Task BroadcastMessage();
    }
}
