﻿using Microsoft.AspNetCore.SignalR;

namespace SignalRAppAPI.Models
{
    public class BroadcastHub : Hub<IHubClient>
    {
    }
}
